import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  ToastAndroid,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';
import {useLoginMutation, MeDocument} from '../../generated/graphql';
import {setAccessToken} from '../../Components/accessToken';
import LoadingComponent from '../../Components/LoadingComponent';
export function navigationOptionsLogin({navigation}) {
  return {
    header: null,
  };
}

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [login] = useLoginMutation();

  return (
    <ImageBackground style={styles.container}>
      <Image
        source={require('../../assets/dori.png')}
        style={styles.logoDori}
      />
      <View style={styles.viewInput}>
        <View>
          <TextInput
            style={styles.inputEmail}
            placeholder={'Email'}
            placeholderTextColor="#000"
            underlineColorAndroid="#D9D9D9"
            onChangeText={text => setEmail(text)}
            autoCompleteType="email"
          />
          <TextInput
            autoCompleteType="password"
            secureTextEntry={true}
            style={styles.inputSenha}
            placeholder={'Senha'}
            placeholderTextColor="#000"
            underlineColorAndroid="#D9D9D9"
            onChangeText={text => setPassword(text)}
          />
          <TouchableOpacity
            style={styles.buttonFacebook}
            onPress={async () => {
              const response = await login({
                variables: {email, password},
                update: (store, {data, errors, loading}) => {
                  if (loading) {
                    return <LoadingComponent />;
                  }

                  if (!data) {
                    return null;
                  }
                  store.writeQuery({
                    query: MeDocument,
                    data: {
                      me: data.login.user,
                    },
                  });
                },
              });

              if (response.data && response) {
                setAccessToken(response.data.login.accessToken);
              }

              navigation.navigate('App');
            }}
            title="Submit">
            <Icon name="facebook" style={styles.iconFacebook} />
            <Text style={styles.textFacebook}>Continue com Facebook</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={styles.buttonInput}
          onPress={async () => {
            const response = await login({
              variables: {email, password},
              update: (store, {data, loading}) => {
                if (loading) {
                  return <LoadingComponent />;
                }
                if (!data) {
                  return null;
                }
                store.writeQuery({
                  query: MeDocument,
                  data: {
                    me: data.login.user,
                  },
                });
              },
            });

            if (response.data && response) {
              setAccessToken(response.data.login.accessToken);
            }

            navigation.navigate('App');
          }}>
          <Text style={styles.textButtonInput}>Entrar</Text>
        </TouchableOpacity>
      </View>
      <Text style={styles.textOu}>Ou</Text>
      <TouchableOpacity onPress={() => navigation.navigate('Cadastro')}>
        <Text style={styles.textSemLogin}>Cadastre-se</Text>
      </TouchableOpacity>
    </ImageBackground>
  );
};

export default Login;

import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP,
  heightPercentageToDP,
} from '../../Components/stylesGlobais';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: null,
    height: null,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoDori: {
    width: widthPercentageToDP('45%'),
    height: widthPercentageToDP('25%'),
    resizeMode: 'contain',
  },

  viewInput: {
    marginTop: heightPercentageToDP('7'),
  },

  inputEmail: {
    width: widthPercentageToDP('80%'),
    borderRadius: 5,
    fontSize: 16,
    paddingBottom: 5,
    paddingBottom: 20,
    paddingLeft: 20,
    paddingHorizontal: 10
  },

  inputSenha: {
    width: widthPercentageToDP('80%'),
    marginTop: 10,
    borderRadius: 5,
    fontSize: 16,
    paddingBottom: 20,
    paddingLeft: 20,
    paddingHorizontal: 10
  },
  buttonInput: {
    marginTop: 10,
    width: widthPercentageToDP('80%'),
    backgroundColor: '#f4511e',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderRadius: 6,
    borderWidth: 0.2,
  },
  textButtonInput: {
    fontSize: 20,
    color: '#fff',
  },
  textOu: {
    marginTop: heightPercentageToDP('1'),
    fontSize: 15,
    alignSelf: 'center',
  },

  buttonFacebook: {
    width: widthPercentageToDP('80%'),
    flexDirection: 'row',
    backgroundColor: '#3b5998',
    marginTop: heightPercentageToDP('1'),
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textFacebook: {
    color: '#fff',
    fontSize: 20,
  },
  iconFacebook: {
    marginRight: 20,
    color: '#fff',
    fontSize: 20,
  },
  textSemLogin: {
    color: '#000',
    fontSize: 15,
    marginTop: 5,
  },
});

export default styles;

import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  TextInput,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';
import {useRegisterMutation} from '../../generated/graphql';

const Cadastro = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [register] = useRegisterMutation();

  return (
    <ImageBackground style={styles.container}>
      <Image
        source={require('../../assets/dori.png')}
        style={styles.logoDori}
      />

      <View style={styles.viewInput}>
        <TextInput
          onChangeText={text => setEmail(text)}
          autoCompleteType="email"
          style={styles.inputEmail}
          placeholder={'E-mail'}
          placeholderTextColor="#000"
          underlineColorAndroid="#D9D9D9"
        />
        <TextInput
          autoCompleteType="password"
          secureTextEntry={true}
          onChangeText={text => setPassword(text)}
          style={styles.inputSenha}
          placeholder={'Senha'}
          placeholderTextColor="#000"
          underlineColorAndroid="#D9D9D9"
        />
        <TouchableOpacity
          onPress={async () => {
            const response = await register({
              variables: {
                email,
                password,
              },
            });
            navigation.goBack();
          }}
          style={styles.buttonInput}>
          <Text style={styles.textButtonInput}>Cadastrar</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

export default Cadastro;

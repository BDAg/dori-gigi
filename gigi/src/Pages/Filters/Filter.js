import React from 'react';
import {FlatList, Image, TouchableOpacity, View} from 'react-native';
import styles from './styles';
import {getAccessToken} from '../../Components/accessToken';

export default class Filters extends React.Component {
  static navigationOptions = {
    title: 'Logo Dori',
    headerTitle: (
      <Image
        source={require('../../assets/dori.png')}
        style={styles.titleImage}
      />
    ),
  };
  renderItem(item) {
    return (
      <TouchableOpacity
        onPress={() => this.props.navigation.navigate('Camera')}>
        <View style={styles.filter}>
          <Image style={styles.filterFoto} source={item.image} />
        </View>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <View style={styles.viewPrincipal}>
        <View style={styles.containerFilter}>
          <FlatList
            style={styles.lista}
            numColumns={3}
            data={[
              {image: require('../../assets/gigi_normal.png'), key: 1},
              {image: require('../../assets/gigi_lingua_de_fora.png'), key: 2},
              {image: require('../../assets/gigi_tattoo.png'), key: 3},
              {image: require('../../assets/gigi_normal.png'), key: 4},
              {image: require('../../assets/gigi_lingua_de_fora.png'), key: 5},
              {image: require('../../assets/gigi_tattoo.png'), key: 6},
              {image: require('../../assets/gigi_normal.png'), key: 7},
              {image: require('../../assets/gigi_lingua_de_fora.png'), key: 8},
              {image: require('../../assets/gigi_tattoo.png'), key: 9},
            ]}
            renderItem={({item}) => this.renderItem(item)}
          />
        </View>
      </View>
    );
  }
}

import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP,
  heightPercentageToDP,
} from '../../Components/stylesGlobais';

const styles = StyleSheet.create({
  viewPrincipal: {
    backgroundColor: '#C60B80',
    width: widthPercentageToDP('100%'),
    height: heightPercentageToDP('100%'),
  },
  lista: {
    flexGrow: 1,
  },
  containerFilter: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#4D6CFA',
  },
  containerFilterColumn: {
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  filter: {
    position: 'relative',
    overflow: 'hidden',
    height: widthPercentageToDP('23%'),
    width: widthPercentageToDP('23%'),
    backgroundColor: '#C30D7E',
    margin: heightPercentageToDP('2.5'),
    borderRadius: 100,
  },
  filterFoto: {
    width: 'auto',
    height: heightPercentageToDP('20%'),
    resizeMode: 'contain',
  },
  titleFilter: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
  },
  titleImage: {
    width: widthPercentageToDP('20%'),
    resizeMode: 'contain',
    marginLeft: widthPercentageToDP('5%'),
  },
});

export default styles;

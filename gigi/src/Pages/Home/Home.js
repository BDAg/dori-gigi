import React from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Button,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';
import {useMeQuery, useLogoutMutation} from '../../generated/graphql';
import {setAccessToken} from '../../Components/accessToken';

export function navigationOptionsHome({navigation}) {
  return {
    title: 'Home',
  };
}

const Home = ({navigation}) => {
  const {data, loading} = useMeQuery();
  const [logout, {client}] = useLogoutMutation();

  let body = null;

  if (loading) {
    body = null;
  } else if (data && data.me) {
    body = <Text>Bem vindo {data.me.email}</Text>;
  } else {
    body = <Text>Usuario nao logado</Text>;
  }

  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.viewPrincipal}>
          <TouchableOpacity
            style={styles.iconCamera}
            onPress={() => navigation.navigate('Camera')}>
            <Icon name="camera" size={42}></Icon>
          </TouchableOpacity>
          <Image
            source={require('../../assets/gigi_normal.png')}
            style={styles.imageGigi}
          />
          {/* botão no meio do nada para teste */}
          <View>
            {!loading && data && data.me ? (
              <Button
                title="Logout"
                onPress={async () => {
                  await logout();
                  setAccessToken('');
                  await client.resetStore();
                  navigation.navigate('Auth');
                }}
              />
            ) : null}
            {body}
          </View>
        </View>
        <View style={styles.viewFilter}>
          <View style={styles.headerFilter}>
            <Text style={styles.titleFilter}>Seus filtros</Text>
          </View>

          <View style={styles.containerFilter}>
            <TouchableOpacity onPress={() => navigation.navigate('Camera')}>
              <View style={styles.filter}>
                <Image
                  style={styles.filterFoto}
                  source={require('../../assets/gigi_normal.png')}
                />
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigation.navigate('Camera')}>
              <View style={styles.filter}>
                <Image
                  style={styles.filterFoto}
                  source={require('../../assets/gigi_tattoo.png')}
                />
              </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigation.navigate('Camera')}>
              <View style={styles.filter}>
                <Image
                  style={styles.filterFoto}
                  source={require('../../assets/gigi_lingua_de_fora.png')}
                />
              </View>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.buttonFilter}
            onPress={() => navigation.navigate('Filters')}>
            <Text style={styles.textButtonFilter}>Veja mais</Text>
            <Icon name="angle-right" size={20} color="#fff" />
          </TouchableOpacity>
        </View>

        <View style={styles.viewTable}>
          <Text style={styles.titleTable}>Classificação</Text>
          <View style={styles.rowTable}>
            <Text style={styles.textTable}>#1</Text>
            <Text style={styles.textTable}>2000</Text>
          </View>
          <View style={styles.rowTable}>
            <Text style={styles.textTable}>#2</Text>
            <Text style={styles.textTable}>1800</Text>
          </View>
          <View style={styles.rowTable}>
            <Text style={styles.textTable}>#3</Text>
            <Text style={styles.textTable}>1830</Text>
          </View>
          <View style={styles.rowTable}>
            <Text style={styles.textTable}>#4</Text>
            <Text style={styles.textTable}>1700</Text>
          </View>
          <View style={styles.rowTable}>
            <Text style={styles.textTable}>#100</Text>
            <Text style={styles.textTable}>900</Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Home;

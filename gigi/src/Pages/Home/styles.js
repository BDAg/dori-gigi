import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP,
  heightPercentageToDP,
} from '../../Components/stylesGlobais';

const styles = StyleSheet.create({

  titleImage: {
    width: widthPercentageToDP('20%'),
    resizeMode: 'contain',
    marginLeft: widthPercentageToDP('5%'),
  },


  container: {
    flexDirection: 'column',
    backgroundColor: 'black',
  },

// PRINCIPAL
  viewPrincipal: {
    backgroundColor: '#C60B80',
    width: widthPercentageToDP('100%'),
    height: heightPercentageToDP('90%'),
  },
  iconCamera: {
    position: 'absolute',
    resizeMode: 'contain',
    marginLeft: widthPercentageToDP('85%'),
    marginTop: heightPercentageToDP('3%'),
  },
  imageGigi: {
    marginLeft: widthPercentageToDP('40%'),
    width: 'auto',
    height: heightPercentageToDP('80%'),
    resizeMode: 'contain',
    transform: [{rotate: '315deg'}],
  },

// FILTROS
  viewFilter: {
    paddingVertical: heightPercentageToDP('2%'),
    backgroundColor: '#fff',
    width: widthPercentageToDP('100%'),
  },
  headerFilter: {
    alignItems:"center"
  },
  titleFilter: {
    letterSpacing: 0.5,
    color: '#333333',
    fontSize: 20,
    fontFamily: 'Poppins-SemiBold',
  },
  containerFilter: {
    marginVertical: heightPercentageToDP('2%'),
    width: widthPercentageToDP('100%'),
    flexDirection: 'row',
    justifyContent: 'center',
  },
  filter: {
    position: 'relative',
    overflow: 'hidden',
    height: widthPercentageToDP('23%'),
    width: widthPercentageToDP('23%'),
    marginHorizontal: heightPercentageToDP('1.5%'),

    backgroundColor: '#C60B80',
    borderRadius: 100,
    borderWidth: 1,
    borderColor: '#D9D9D9',
  },
  filterFoto: {
    width: 'auto',
    height: heightPercentageToDP('20%'),
    resizeMode: 'contain',
  },
  buttonFilter: {
    width: widthPercentageToDP('40%'),
    alignSelf: 'center',
    padding: 4,
    borderRadius: 5,
    backgroundColor: '#f4511e',
    marginTop: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textButtonFilter: {
    textAlign: 'center',
    fontSize: 15,
    color: '#fff',
    fontFamily: 'Poppins-Regular',
    marginRight: 8
  },

// TABELA
  viewTable: {
    backgroundColor: "#009fe3",
    paddingVertical: 15,
    paddingHorizontal: widthPercentageToDP('10%'),
  },
  titleTable:{
    fontSize: 20,
    fontFamily: 'Poppins-SemiBold',
    letterSpacing: 0.5,
    color: '#fff',
    alignSelf: 'center',
    marginBottom: 15
  },
  rowTable:{
    backgroundColor: '#007eaf',
    width: widthPercentageToDP('80%'),
    flexDirection: 'row',
    borderRadius: 5,
    marginBottom: 8
  },
  textTable: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingVertical: 20,
    color: '#fff',
    width: widthPercentageToDP('40%'),
    textAlign: 'center',
    
  }
});

export default styles;

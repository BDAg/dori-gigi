import {View, ActivityIndicator, StatusBar, StyleSheet} from 'react-native';
import React, {Component} from 'react';
import {getAccessToken} from '../../Components/accessToken';

export default class AuthLoading extends Component {
  componentDidMount() {
    this._bootstrapAsync();
  }

  _bootstrapAsync = async () => {
    const userToken = getAccessToken();
    if (!userToken) {
      this.props.navigation.navigate('Auth');
    } else {
      this.props.navigation.navigate('App');
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import AuthLoading from './Pages/AuthLoading/AuthLoading';
import {AppStack} from './Routes/AppStack';
import {AuthStack} from './Routes/AuthStack';

export const CreateRootNavigator = createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoading,
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    },
  ),
);

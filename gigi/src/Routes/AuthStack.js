import Login, {navigationOptionsLogin} from '../Pages/Login/Login';
import Cadastro from '../Pages/Cadastro/Cadastro';
import {createStackNavigator} from 'react-navigation-stack';
import React from 'react';
import {Image} from 'react-native';
import {styles} from '../Components/stylesGlobais';

export const AuthStack = createStackNavigator(
  {
    Login: {screen: Login, navigationOptions: navigationOptionsLogin},
    Cadastro: {screen: Cadastro},
  },
  {
    initialRouteName: 'Login',
    defaultNavigationOptions: {
      headerTitle: (
        <Image
          source={require('../assets/dori.png')}
          style={styles.titleImage}
        />
      ),
      headerStyle: {
        backgroundColor: '#fff',
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);

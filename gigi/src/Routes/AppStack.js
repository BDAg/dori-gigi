import Home from '../Pages/Home/Home';
import Filters from '../Pages/Filters/Filter';
import {createStackNavigator} from 'react-navigation-stack';
import React from 'react';
import {Image} from 'react-native';
import Camera from '../Components/Camera';
import {styles} from '../Components/stylesGlobais';

export const AppStack = createStackNavigator(
  {
    Home: Home,
    Filters: Filters,
    Camera: Camera,
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerTitle: (
        <Image
          source={require('../assets/dori.png')}
          style={styles.titleImage}
        />
      ),
      headerStyle: {
        backgroundColor: '#fff',
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);

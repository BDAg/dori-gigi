import React, {useState} from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import Share from 'react-native-share';

const ImageToShare = ({uri}) => {
  const [result, setResult] = useState('');
  function getErrorString(error, defaultValue) {
    let e = defaultValue || 'Algo deu errado tente novamente';
    if (typeof error === 'string') {
      e = error;
    } else if (error && error.message) {
      e = error.message;
    } else if (error && error.props) {
      e = error.props;
    }
    return e;
  }

  const shareSingleImage = async () => {
    const shareOptions = {
      title: 'DORI GIGI',
      message: 'COMPARTILHADO PELO APP DA GIGI',
      social: Share.Social.INSTAGRAM,
      type: 'image/jpeg',
      url: `file://${uri}`,
      failOnCancel: false,
      forceDialog: true,
    };

    try {
      const ShareResponse = await Share.shareSingle(shareOptions);
      setResult(JSON.stringify(ShareResponse, null, 2));
    } catch (error) {
      console.log('Error =>', error);
      setResult('error: '.concat(getErrorString(error)));
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.button}>
        <Button onPress={shareSingleImage} title="Compartilhar" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    marginBottom: 10,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});

export default ImageToShare;

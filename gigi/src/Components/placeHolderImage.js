import React, {Component} from 'react';
import {View, Image, ImageBackground, Animated, Text} from 'react-native';
import ImageToShare from './imageToShare';

export default class PlaceholderImage extends Component {
  // state = {
  //   opacity: new Animated.Value(5),
  // };

  // onLoad = event => {
  //   Animated.timing(this.state.opacity, {
  //     toValue: 1,
  //     duration: 500,
  //   }).start();
  // };

  render() {
    return (
      <View
        style={{
          backgroundColor: 'purple',
          width: this.props.style.width || '100%',
          height: this.props.style.height || '100%',
        }}>
        <Text
          style={{
            flex: 1,
            textAlign: 'center',
            textAlignVertical: 'center',
            fontSize: 35,
            color: '#fff',
            fontFamily: 'Poppins-Regular',
          }}>
          Carregando
        </Text>
        <ImageBackground
          {...this.props}
          style={[this.props.style, {position: 'absolute'}]}>
          <ImageToShare uri={this.props.source.uri} />
        </ImageBackground>
      </View>
    );
  }
}

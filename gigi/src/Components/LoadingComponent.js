import React from 'react';
import {View, ActivityIndicator} from 'react-native';

const LoadingComponent = () => {
  return (
    <View style={{justifyContent: 'center', alignContent: 'center', flex: 1}}>
      <ActivityIndicator />
    </View>
  );
};

export default LoadingComponent;

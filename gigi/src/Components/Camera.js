import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Image} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/FontAwesome';
import {widthPercentageToDP, heightPercentageToDP} from './stylesGlobais';
import PlaceholderImage from './placeHolderImage';
import ImageToShare from './imageToShare';

export default class Camera extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageUri: '',
      base64: '',
    };
  }
  static navigationOptions = {header: null};
  render() {
    console.log(this.state.imageUri);
    if (this.state.imageUri) {
      return (
        // <ImageToShare imageBase64={this.state.imageUri} />
        <PlaceholderImage
          source={{uri: this.state.imageUri}}
          style={{width: '100%', height: '100%'}}
          // base64Image={this.state.base64}
        />
      );
    }

    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.front}
          flashMode={RNCamera.Constants.FlashMode.off}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />
        <View style={styles.viewCapture}>
          <TouchableOpacity
            onPress={this.takePicture.bind(this)}
            style={styles.capture}>
            {/* <Icon name="circle" size={20}/> */}
            <Text>Foto</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  takePicture = async () => {
    if (this.camera) {
      const data = await this.camera.takePictureAsync({base64: true});
      this.setState({imageUri: data.uri, base64: data.base64});
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  viewCapture: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  capture: {
    backgroundColor: '#fff',
    borderRadius: 100,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});

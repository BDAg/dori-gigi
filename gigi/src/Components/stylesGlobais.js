import {Dimensions, PixelRatio, StyleSheet} from 'react-native';

export const widthPercentageToDP = widthPercent => {
  const screenWidth = Dimensions.get('window').width;
  return PixelRatio.roundToNearestPixel(
    (screenWidth * parseFloat(widthPercent)) / 100,
  );
};

export const heightPercentageToDP = heightPercent => {
  const screenHeight = Dimensions.get('window').height;
  return PixelRatio.roundToNearestPixel(
    (screenHeight * parseFloat(heightPercent)) / 100,
  );
};

export const styles = StyleSheet.create({
  titleImage: {
    width: widthPercentageToDP('20%'),
    resizeMode: 'contain',
    marginLeft: widthPercentageToDP('5%'),
  },
});

import React, {useState, useEffect} from 'react';
import {CreateRootNavigator} from './routes';
import {setAccessToken} from './Components/accessToken';
import LoadingComponent from './Components/LoadingComponent';

const App = () => {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    fetch('http://192.168.86.7:4000/refresh_token', {
      method: 'POST',
      credentials: 'include',
    }).then(async x => {
      const {accessToken} = await x.json();
      setAccessToken(accessToken);
      setLoading(false);
    });
  }, []);

  if (loading) {
    return <LoadingComponent />;
  } else {
    return <CreateRootNavigator />;
  }
};

export default App;

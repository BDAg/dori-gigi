import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BaseEntity,
  ManyToMany,
  JoinTable
} from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";
import { Filters } from "./Filter";

@ObjectType()
@Entity("campaigns")
export class Campaign extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column("text")
  name: string;

  @Column()
  lancamento: Date;

  @ManyToMany(() => Filters)
  @JoinTable()
  filters: Filters[];
}
